package com.orsyp.tools.packageexporter.api;

import java.util.ArrayList;
import java.util.List;
import com.orsyp.Environment;
import com.orsyp.Identity;
import com.orsyp.SyntaxException;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.deploymentpackage.ApiPackageType;
import com.orsyp.api.deploymentpackage.PackageItem;
import com.orsyp.api.deploymentpackage.PackageList;
import com.orsyp.api.security.Operation;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.MultiCentralConnectionFactory;
import com.orsyp.std.central.UniCentralStdImpl;
import com.orsyp.std.deploymentpackage.PackageListStdImpl;
import com.orsyp.std.deploymentpackage.PackageStdImpl;
import com.orsyp.api.deploymentpackage.Package;

public class UvmsApiConnection {
	
	private Context centralContext;
	
	public UvmsApiConnection(String host, int port, String user, String password) {
		try {
			getUVMSConnection(host, port, user, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	private UniCentral getUVMSConnection(String host, int port, String user, String password) throws SyntaxException {		
		UniCentral cent = new UniCentral(host, port);
		cent.setImplementation(new UniCentralStdImpl(cent));
		
		Context ctx = new Context(new Environment("UJCENT",host), new Client(new Identity(user, password, host, "")));
		ctx.setProduct(com.orsyp.api.Product.UNICENTRAL);
		ctx.setUnijobCentral(cent);
		ClientServiceLocator.setContext(ctx);

		try {
			cent.login(user, password);
			if (ClientConnectionManager.getDefaultFactory() == null) {
	            ClientConnectionManager.setDefaultFactory(MultiCentralConnectionFactory.getInstance());
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		centralContext=ctx;
		
		return cent;
	}

	
	//-------------------------------------------
	
	public List<String> getPackageList() throws Exception {
		
		ArrayList<String> list = new ArrayList<String>();
		
		PackageList packageList = new PackageList(centralContext, PRODUCT_TYPE.DU_OWLS);
	    packageList.setImpl(new PackageListStdImpl());
	    packageList.extract(Operation.DISPLAY);
	    for (int i=0; i<packageList.getCount(); i++){
	    	PackageItem item = packageList.get(i);
	    	if (item.getPackageType()==ApiPackageType.DESIGN_OBJECTS)
	    		list.add(item.getName());
	    }
		return list;
	}

	public Package getPackage(String name) throws Exception{
		Package p = new Package();
	    p.setName(name);	    
	    p.setContext(centralContext);
	    p.setProduct(PRODUCT_TYPE.DU_OWLS);
    	p.setType(ApiPackageType.DESIGN_OBJECTS);
	    p.setImpl(new PackageStdImpl());
	    p.extract();
		return p;
	}
		
}
