package com.orsyp.tools.packageexporter.gui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import java.awt.CardLayout;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JPasswordField;

import com.orsyp.api.deploymentpackage.Package;
import com.orsyp.api.deploymentpackage.PackageElement;
import com.orsyp.tools.packageexporter.PkgExport;
import com.orsyp.tools.packageexporter.Version;
import com.orsyp.tools.packageexporter.PkgExport.ExportFormat;
import com.orsyp.tools.packageexporter.api.UvmsApiConnection;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import javax.swing.JList;
import javax.swing.JScrollPane;
import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.Toolkit;

public class PackageExporterGui extends JFrame {
	private static final long serialVersionUID = 1L;

	private static final String CONFIGFILE = "pkgexporter.conf";
	
	private JPanel contentPane;
	private JPanel pnPackages;
	private JTextField txServer;
	private JTextField txPort;
	private JTextField txUser;
	private JPasswordField txPwd;
	private CardLayout cardLayout;
	
	private JRadioButton rbJson;
	private JRadioButton rbXml;
	private JRadioButton rbCustom;
	
	private JFrame frame;
	private JList list;
	
	private UvmsApiConnection conn;
	private List<String> packages;
	private Package pkg;
	private JTextField txFile;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	
	private Properties pr = new Properties();
	
	private PkgExport exporter;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PackageExporterGui frame = new PackageExporterGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PackageExporterGui() {
		try {
			UIManager.setLookAndFeel("com.nilo.plaf.nimrod.NimRODLookAndFeel");
		} catch (Exception e) {}
		
		setIconImage(Toolkit.getDefaultToolkit().getImage("img/du.png"));
		
		frame = this;
		setTitle("Orsyp Package Exporter - V" + Version.VERSION);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(480, 340, 480, 340);
		setMinimumSize(new Dimension(480, 340));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		cardLayout = new CardLayout(0, 0);
		contentPane.setLayout(cardLayout);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, "connection");
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(UIManager.getColor("control"));
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		panel.add(panel_1, BorderLayout.SOUTH);		
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2, BorderLayout.CENTER);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{90, 0, 6, 0};
		gbl_panel_2.rowHeights = new int[]{30, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0};
		gbl_panel_2.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		JLabel lblUvmsServer = new JLabel("UVMS Server");
		lblUvmsServer.setVerticalAlignment(SwingConstants.BOTTOM);
		GridBagConstraints gbc_lblUvmsServer = new GridBagConstraints();
		gbc_lblUvmsServer.anchor = GridBagConstraints.WEST;
		gbc_lblUvmsServer.insets = new Insets(0, 0, 5, 5);
		gbc_lblUvmsServer.gridx = 1;
		gbc_lblUvmsServer.gridy = 1;
		panel_2.add(lblUvmsServer, gbc_lblUvmsServer);
		
		txServer = new JTextField();
		txServer.setPreferredSize(new Dimension(200, 25));
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.anchor = GridBagConstraints.WEST;
		gbc_textField.gridx = 3;
		gbc_textField.gridy = 1;
		panel_2.add(txServer, gbc_textField);
		txServer.setPreferredSize(new Dimension(200, 25));
		
		JLabel lblPort = new JLabel("Port");
		lblPort.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblPort = new GridBagConstraints();
		gbc_lblPort.anchor = GridBagConstraints.WEST;
		gbc_lblPort.insets = new Insets(0, 0, 5, 5);
		gbc_lblPort.gridx = 1;
		gbc_lblPort.gridy = 2;
		panel_2.add(lblPort, gbc_lblPort);
		
		txPort = new JTextField();
		txPort.setText("4184");
		txPort.setPreferredSize(new Dimension(200, 25));
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 0);
		gbc_textField_1.anchor = GridBagConstraints.WEST;
		gbc_textField_1.gridx = 3;
		gbc_textField_1.gridy = 2;
		panel_2.add(txPort, gbc_textField_1);
		
		JLabel lblUser = new JLabel("User");
		lblUser.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblUser = new GridBagConstraints();
		gbc_lblUser.anchor = GridBagConstraints.WEST;
		gbc_lblUser.insets = new Insets(0, 0, 5, 5);
		gbc_lblUser.gridx = 1;
		gbc_lblUser.gridy = 3;
		panel_2.add(lblUser, gbc_lblUser);
		
		txUser = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.anchor = GridBagConstraints.WEST;
		gbc_textField_2.insets = new Insets(0, 0, 5, 0);
		gbc_textField_2.gridx = 3;
		gbc_textField_2.gridy = 3;
		panel_2.add(txUser, gbc_textField_2);
		txUser.setPreferredSize(new Dimension(200, 25));
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblPassword = new GridBagConstraints();
		gbc_lblPassword.anchor = GridBagConstraints.WEST;
		gbc_lblPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblPassword.gridx = 1;
		gbc_lblPassword.gridy = 4;
		panel_2.add(lblPassword, gbc_lblPassword);
		
		txPwd = new JPasswordField();
		txPwd.setPreferredSize(new Dimension(200, 25));
		GridBagConstraints gbc_passwordField = new GridBagConstraints();
		gbc_passwordField.anchor = GridBagConstraints.WEST;
		gbc_passwordField.insets = new Insets(0, 0, 5, 0);
		gbc_passwordField.gridx = 3;
		gbc_passwordField.gridy = 4;
		panel_2.add(txPwd, gbc_passwordField);
				
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(211, 211, 211));
		panel.add(panel_3, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("UVMS Server Connection");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel_3.add(lblNewLabel);
		
		
		JButton btnConnect = new JButton("Connect   >>", new ImageIcon("img/plug.png"));
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					connect(txServer.getText(), txPort.getText(), txUser.getText(), String.valueOf(txPwd.getPassword()));
					frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				} catch (Exception e) {
					e.printStackTrace();
					frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					JOptionPane.showMessageDialog(frame, getExString(e), "Connection error" , JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel_1.add(btnConnect);
		
		
		
		pnPackages = new JPanel();
		contentPane.add(pnPackages, "packages");
		pnPackages.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(new Color(211, 211, 211));
		pnPackages.add(panel_4, BorderLayout.NORTH);
		
		JLabel lblAvailablePackages = new JLabel("Select package");
		lblAvailablePackages.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel_4.add(lblAvailablePackages);
		
		JPanel panel_5 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_5.getLayout();
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		pnPackages.add(panel_5, BorderLayout.SOUTH);
		
		JButton btnGetPackageData = new JButton("Get package data   >>", new ImageIcon("img/download.png"));
		btnGetPackageData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (list.getSelectedIndex()>=0) {
					try {
						frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
						getPackage((String)list.getSelectedValue());
						frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					} catch (Exception e) {
						frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
						JOptionPane.showMessageDialog(frame, "Error getting package\n" + getExString(e), "Error" , JOptionPane.ERROR_MESSAGE);
					}
				}
				else
					JOptionPane.showMessageDialog(frame, "Select a package from the list", "Info" , JOptionPane.INFORMATION_MESSAGE);
			}
		});
		panel_5.add(btnGetPackageData);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new EmptyBorder(5, 10, 5, 10));
		pnPackages.add(panel_6, BorderLayout.CENTER);
		panel_6.setLayout(new BorderLayout(10, 10));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_6.add(scrollPane, BorderLayout.CENTER);
		
		list = new JList();
		scrollPane.setViewportView(list);
		
		JPanel pnExport = new JPanel();
		contentPane.add(pnExport, "export");
		pnExport.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_7 = new JPanel();
		panel_7.setBackground(new Color(211, 211, 211));
		pnExport.add(panel_7, BorderLayout.NORTH);
		
		JLabel lblExportOptions = new JLabel("Export options");
		lblExportOptions.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel_7.add(lblExportOptions);
		
		JPanel panel_8 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_8.getLayout();
		flowLayout_2.setAlignment(FlowLayout.RIGHT);
		pnExport.add(panel_8, BorderLayout.SOUTH);
		
		JButton btnExport = new JButton("Export   >>", new ImageIcon("img/export.png"));
		btnExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (txFile.getText().length()==0) {
					JOptionPane.showMessageDialog(frame, "Select a destination file", "Warning" , JOptionPane.WARNING_MESSAGE);
					return;
				}
				
				try {
					frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					ExportFormat format = ExportFormat.CUSTOM;
					if (rbXml.isSelected())
						format = ExportFormat.XML;
					else
					if (rbJson.isSelected())
						format = ExportFormat.JSON;						
					exporter.export(txFile.getText(), format);
					cardLayout.show(contentPane, "done");
					frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				} catch (Exception ex) {
					ex.printStackTrace();
					frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					JOptionPane.showMessageDialog(frame, "Error exporting package:\n" + getExString(ex), "Error" , JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel_8.add(btnExport);
		
		JPanel panel_9 = new JPanel();
		pnExport.add(panel_9, BorderLayout.CENTER);
		panel_9.setLayout(null);
		
		JLabel lblSelectFile = new JLabel("Select file");
		lblSelectFile.setBounds(55, 32, 85, 14);
		panel_9.add(lblSelectFile);
		
		txFile = new JTextField();
		txFile.setBounds(55, 49, 302, 25);
		panel_9.add(txFile);
		txFile.setColumns(10);
		
		JButton button = new JButton("...");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser c = new JFileChooser();
				int rVal = c.showSaveDialog(frame);
			    if (rVal == JFileChooser.APPROVE_OPTION)	
			    	txFile.setText(c.getSelectedFile().getPath());
			}
		});
		button.setBounds(367, 48, 39, 23);
		panel_9.add(button);
		
		JLabel lblSelectFormat = new JLabel("Select format");
		lblSelectFormat.setBounds(55, 98, 85, 14);
		panel_9.add(lblSelectFormat);
		
		rbJson = new JRadioButton("JSON");
		rbJson.setSelected(true);
		buttonGroup.add(rbJson);
		rbJson.setBounds(65, 119, 109, 23);
		panel_9.add(rbJson);
		
		rbXml = new JRadioButton("XML");
		buttonGroup.add(rbXml);
		rbXml.setBounds(65, 145, 109, 23);
		panel_9.add(rbXml);
		
		rbCustom = new JRadioButton("Custom text format");
		buttonGroup.add(rbCustom);
		rbCustom.setBounds(65, 171, 169, 23);
		panel_9.add(rbCustom);
		
		JPanel pnDone = new JPanel();
		contentPane.add(pnDone, "done");
		pnDone.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_10 = new JPanel();
		panel_10.setBackground(new Color(211, 211, 211));
		pnDone.add(panel_10, BorderLayout.NORTH);
		
		JLabel lblDone = new JLabel("Done");
		lblDone.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel_10.add(lblDone);
		
		JPanel panel_11 = new JPanel();
		FlowLayout flowLayout_3 = (FlowLayout) panel_11.getLayout();
		flowLayout_3.setAlignment(FlowLayout.RIGHT);
		pnDone.add(panel_11, BorderLayout.SOUTH);
		
		
		JButton btnRestart = new JButton("Export another package", new ImageIcon("img/refresh.png"));
		btnRestart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					pr.setProperty("server", txServer.getText());
					pr.setProperty("port", txPort.getText());
					pr.setProperty("user", txUser.getText());					
					pr.store(new FileOutputStream("pkgexporter.conf"),null);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				cardLayout.show(contentPane, "connection");
			}
		});
		panel_11.add(btnRestart);
		
		
		JButton btnClose = new JButton("Close", new ImageIcon("img/done.png"));
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					pr.setProperty("server", txServer.getText());
					pr.setProperty("port", txPort.getText());
					pr.setProperty("user", txUser.getText());					
					pr.store(new FileOutputStream(CONFIGFILE),null);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				System.exit(0);
			}
		});
		panel_11.add(btnClose);
		
		JLabel lblNewLabel_1 = new JLabel("Export completed successfully.");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		pnDone.add(lblNewLabel_1, BorderLayout.CENTER);
		
		try {
			pr.load(new FileInputStream(CONFIGFILE));
		} catch (Exception e1) {}
		
		txServer.setText(pr.getProperty("server"));
		txPort.setText(pr.getProperty("port"));
		txUser.setText(pr.getProperty("user"));
		txPwd.setText("");
	}


	protected String getExString(Exception ex) {
		String tx = ex.getMessage();
		if (tx==null) {
			tx = ex.getClass().getSimpleName();
			tx += "\n"+ex.getStackTrace()[0].toString();
		}
		return tx;
	}

	protected void getPackage(String pkgName) throws Exception {
		exporter = new PkgExport();
		pkg = conn.getPackage(pkgName);
		for (PackageElement pe : pkg.getElements()) {
			switch (pe.getType()) {
				case UPROC:		exporter.addUproc(pe.getUproc());
								break;
				case TASK:		exporter.addTask(pe.getTask());	
								break;
				case SESSION:	exporter.addSession(pe.getSession());
								break;
				case RESOURCE:	exporter.addResource(pe.getResource());
								break;
				case BUSINESS_VIEW: exporter.addBV(pe.getBusinessView());
									break;
			}
		}
		
		if (exporter.getTotalCount()==0)
			JOptionPane.showMessageDialog(frame, "No objects found in package " + pkgName, "Warning" , JOptionPane.WARNING_MESSAGE);
		else
			cardLayout.show(contentPane, "export");
	}

	protected void connect(String srv, String port, String user, String pwd) throws Exception {
		conn = new UvmsApiConnection(srv, Integer.parseInt(port), user, pwd);
		packages = conn.getPackageList();
		Collections.sort(packages);
		DefaultListModel model = new DefaultListModel();
		for (String p: packages) 
			model.addElement(p);
		list.setModel(model);		
		cardLayout.show(contentPane, "packages");
	}
}
