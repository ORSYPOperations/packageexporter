package com.orsyp.tools.packageexporter;

import java.util.Collections;
import java.util.List;
import com.orsyp.api.deploymentpackage.Package;
import com.orsyp.api.deploymentpackage.PackageElement;
import com.orsyp.tools.packageexporter.PkgExport.ExportFormat;
import com.orsyp.tools.packageexporter.api.UvmsApiConnection;

public class PkgExporter {
	
	private static String HELP = "Usage:\npkgExporter <uvms server address> <port> <user> <password> <package name> <output format (XML or JSON or CUSTOM)> <output file name>";
	
	private static String outputFormat;

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		
		System.out.println("ORSYP Package Exporter V" + Version.VERSION);
		
		if (args.length<7) {
			System.out.println(HELP);
			return;
		}
			
		String server = args[0];
		String port = args[1];
		String user = args[2];
		String pw = args[3];
		
		String packageName = args[4];
		
		outputFormat = args[5];
		
		ExportFormat format = ExportFormat.CUSTOM; 
		if (outputFormat.equalsIgnoreCase("xml"))
			format = ExportFormat.XML;
		else
		if (outputFormat.equalsIgnoreCase("json"))
			format = ExportFormat.JSON;
		else
		if (outputFormat.equalsIgnoreCase("custom"))
			format = ExportFormat.CUSTOM;	
			
		
		String file = args[6];
		
		if (!outputFormat.equalsIgnoreCase("xml") && !outputFormat.equalsIgnoreCase("json") && !outputFormat.equalsIgnoreCase("custom")) {
			System.out.println("Unknown output format: " + outputFormat);
			System.out.println(HELP);
			return;
		}
		
		System.out.println("Connecting to " + server + "...");
		UvmsApiConnection conn = new UvmsApiConnection(server, Integer.parseInt(port), user, pw);
		System.out.println("Reading package list ...");
		List<String> l = conn.getPackageList();
		Collections.sort(l);
		
		PkgExport exporter = new PkgExport();
		
		System.out.println("Reading package "+packageName+" ...");
		Package p = conn.getPackage(packageName);
		
		for (PackageElement pe : p.getElements()) {
			switch (pe.getType()) {
				case UPROC:		exporter.addUproc(pe.getUproc());
								break;
				case TASK:		exporter.addTask(pe.getTask());	
								break;
				case SESSION:	exporter.addSession(pe.getSession());
								break;
				case RESOURCE:	exporter.addResource(pe.getResource());
								break;
				case BUSINESS_VIEW: exporter.addBV(pe.getBusinessView());
									break;
			}
		}
		
		System.out.println("Exporting as "+outputFormat+" to file "+file+" ...");
				
		exporter.export(file, format);
		
		System.out.println("Done");
	}

}
