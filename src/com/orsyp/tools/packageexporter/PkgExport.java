package com.orsyp.tools.packageexporter;

import java.io.File;
import java.io.PrintWriter;
import java.util.TreeMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orsyp.api.bv.BusinessView;
import com.orsyp.api.resource.Resource;
import com.orsyp.api.session.Session;
import com.orsyp.api.task.Task;
import com.orsyp.api.task.TaskImplicitData;
import com.orsyp.api.uproc.Uproc;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;

public class PkgExport {
	
	public enum ExportFormat {XML, JSON, CUSTOM};

	private TreeMap<String,Uproc> uprocs = new TreeMap<String, Uproc>();
	private TreeMap<String,Session> sessions = new TreeMap<String, Session>();
	private TreeMap<String,Task> tasks = new TreeMap<String, Task>();
	private TreeMap<String,Resource> resources = new TreeMap<String, Resource>();
	private TreeMap<String,BusinessView> bvs = new TreeMap<String, BusinessView>();
	
	private Vector<String> usedTasks = new Vector<String>();
	private Vector<String> usedSessions = new Vector<String>();
	private Vector<String> usedUprocs = new Vector<String>();
	private Vector<String> usedResources = new Vector<String>();
	
	private FieldOrder order = new FieldOrder();
	
	
	public void addBV(BusinessView b) {
		bvs.put(b.getName(), b);
	}
	
	public void addTask(Task t) {
		tasks.put(t.getIdentifier().getName(), t);	
	}
	
	public void addSession(Session s) {
		sessions.put(s.getName(), s);
	}
	
	public void addUproc(Uproc u) {
		uprocs.put(u.getName(), u);
	}
	
	public void addResource(Resource r) {
		resources.put(r.getIdentifier().getName(), r);
	}
	
	
	public void export(String file, ExportFormat format) throws Exception{
		XStream xs;
		if (format == ExportFormat.XML)
			xs = new XStream();
		else			
			xs = new XStream(new JsonHierarchicalStreamDriver());
		
		PrintWriter f = new PrintWriter(new File(file));
		
		//cycle bvs
		for (BusinessView b : bvs.values()) { 
			f.write(formatText(xs.toXML(b),format)+"\n");
			
			//read bv tasks
			TreeMap<String,Task> bvTasks = new TreeMap<String, Task>();
			
			String xmlTxt = "";
			for (String str : b.getData().getLines()) 
				xmlTxt += "\n"+str;
			
			Vector<String> matches = getXmlMatches(xmlTxt,"taskName");
			for (String m : matches) {
				Task t = tasks.get(m);
				if (t!=null) 
					bvTasks.put(t.getIdentifier().getName(), t);
			}
					
			//cycle bv tasks
			for (Task t : bvTasks.values()) {
				writeTask(t,f,xs,format);								
			}
			
			//bv read resources			
			xmlTxt = b.getData().getLines().toString();			
			matches = getXmlMatches(xmlTxt,"resource");
			for (String m : matches) {
				Resource r = resources.get(m);
				if (r!=null) {
					if (!usedResources.contains(r.getIdentifier().getName()))
						usedResources.add(r.getIdentifier().getName());
					f.write(formatText(xs.toXML(r),format)+"\n");
				}
			}			
		}

		for (Task t : tasks.values())
			if (!usedTasks.contains(t.getIdentifier().getName())) 
				writeTask(t,f,xs,format);
		
		for (Session s : sessions.values())
			if (!usedSessions.contains(s.getName())) 
				writeSession(s, f, xs, format);
		
		for (Uproc u : uprocs.values())
			if (!usedUprocs.contains(u.getName())) 
				writeUproc(u, f, xs, format);
		
		for (Resource r : resources.values())
			if (!usedResources.contains(r.getIdentifier().getName())) 
				f.write(formatText(xs.toXML(r),format)+"\n");
		
		TreeMap<String,TaskImplicitData> rules = new TreeMap<String,TaskImplicitData>();
		for (Task t : tasks.values()) 
			if (t.getPlanifiedData()!=null)
				for (TaskImplicitData data : t.getPlanifiedData().getImplicitData()) {
					String r = data.getIdentifier().getName();
					if (!rules.containsKey(r)) 
						rules.put(r, data);
				}
		
		for (TaskImplicitData data : rules.values()) 
			f.write(formatText(xs.toXML(data),format)+"\n");
		
		f.close();
	}
	
	private void writeTask(Task t, PrintWriter f, XStream xs, ExportFormat format) {
		f.write(formatText(xs.toXML(t),format)+"\n");
		
		if (!usedTasks.contains(t.getIdentifier().getName()))
			usedTasks.add(t.getIdentifier().getName());
			
		Session s = sessions.get(t.getSessionName());
		if (s!=null) {			
			writeSession(s,f,xs,format);			
		} else {
			Uproc u = uprocs.get(t.getSessionName());
			if (u!=null) {
				if (!usedUprocs.contains(u.getName()))
					usedUprocs.add(u.getName());
				f.write(formatText(xs.toXML(u),format)+"\n");
			}
		}
	}

	private void writeSession(Session s, PrintWriter f, XStream xs, ExportFormat format) {
		if (!usedSessions.contains(s.getName()))
			usedSessions.add(s.getName());
		
		//print session
		f.write(formatText(xs.toXML(s),format)+"\n");
		
		//read session uprocs
		TreeMap<String,Uproc> sesUprocs = new TreeMap<String, Uproc>();
		for (String uprName :s.getUprocs()) {
			Uproc u = uprocs.get(uprName);
			if (u!=null) 
				sesUprocs.put(u.getName(), u);
		}
		//write uprocs
		for (Uproc u : sesUprocs.values()) 
			writeUproc(u,f,xs,format);
	}

	private void writeUproc(Uproc u, PrintWriter f, XStream xs, ExportFormat format) {
		if (!usedUprocs.contains(u.getName()))
			usedUprocs.add(u.getName());
		f.write(formatText(xs.toXML(u),format)+"\n");
	}

	private String formatText(String txt, ExportFormat format) {
		if (format!=ExportFormat.XML) {
			Gson gs = new GsonBuilder().setPrettyPrinting().create();
			String s = gs.toJson(gs.fromJson(txt, Object.class));
			if (format==ExportFormat.CUSTOM) {
				s +="\n"; //to avoid processing \z separately with another regex 
				// produce plain text
				//remove { and }				
				//s = s.replaceAll("\\}\\z", "\n");
				s = s.replaceAll("\\}\\s*\\n", "\n");
				s = s.replace("},\n", "\n");				
				s = s.replace("{\n", "\n");
				
				//remove first " without losing indenting
				Pattern p1 = Pattern.compile("\\n\\s+\"");
			    Matcher m1 = p1.matcher(s);
			    StringBuffer sb1 = new StringBuffer(s.length());
			    while (m1.find()) {
			      String st = m1.group(0);
			      m1.appendReplacement(sb1, Matcher.quoteReplacement(st.replace("\"", "")));
			    }
			    m1.appendTail(sb1);				
			    s= sb1.toString();
			    
			    //remove other "
				s = s.replaceAll("\\\"\\: +",": ");
				s = s.replaceAll("\\:\\s+\"",": ");
				s = s.replaceAll("\\\"\\s*\\n","\n");
				//s = s.replaceAll("\\\"\\s*\\z","");
				s = s.replaceAll("\\\",\\s*\\n","\n");
				
				
				//remove ,
				s = s.replace(",\n", "\n");
				
				//remove [ and ]
				s = s.replace("[]\n", "\n");		
				//s = s.replaceAll("\\]\\s*\\z", "\n");
				s = s.replaceAll("\\]\\s*\\n", "\n");
				s = s.replaceAll("\\[\\s*\\n", "\n");
								
				
				//remove some fields
				s = s.replaceAll("\\s*@class[^\\n]*", "");
				s = s.replaceAll("\\s*@reference[^\\n]*", "");
				s = s.replaceAll("\\s*syntaxRules:[^\\n]*", "");
				s = s.replaceAll("\\s*com\\.orsyp\\.api\\.[a-z]+\\.", "");
				s = s.replaceAll("\\s*syntaxCheck\\:[^\\n]*", "");
				s = s.replaceAll("\\s*current\\: true[^\\n]*", "");
				s = s.replaceAll("\\s*previous\\: false[^\\n]*", "");
				
				//remove empty lines
				s = s.replaceAll("\\n\\W*\\n", "\n");
				
				//replace .0 in numbers
				s = s.replaceAll("(?<=[0-9])\\.0[\\r\\n]", "\n");
				
				//replace utf codes with char
				if (s.contains("\\u")) {
					Pattern p = Pattern.compile("\\\\u[0-9a-f]{4}");
				    Matcher m = p.matcher(s);
				    StringBuffer sb = new StringBuffer(s.length());
				    while (m.find()) {
				      String utf = m.group(0);
				      int hexToInt = Integer.parseInt(utf.substring(2,6), 16);
				      m.appendReplacement(sb, Matcher.quoteReplacement(Character.toString((char)hexToInt)));
				    }
				    m.appendTail(sb);
					
				    s= sb.toString().trim();
				}
				
				s = s.replace("TaskImplicitData:", "Rule:");
				
				s = order.reduce(s);
				
				s = order.sort(s);
			}			
			
			return s;
		}
		else
			return txt;
	}

	private Vector<String> getXmlMatches (String txt, String tag) {
		Vector<String> v = new Vector<String>();
		
		Pattern expr = Pattern.compile("(?<=" + tag + "=\\\")[^\\\"]+");
		Matcher matcher = expr.matcher(txt);
		while ( matcher.find() )                 
			v.add(matcher.group());
		
		return v;
	}

	public int getTotalCount() {
		return uprocs.size() + sessions.size() + tasks.size() + bvs.size() + resources.size() ;
	}

}
