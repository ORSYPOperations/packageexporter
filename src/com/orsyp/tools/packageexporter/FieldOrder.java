package com.orsyp.tools.packageexporter;

import java.util.HashMap;
import java.util.TreeMap;

public class FieldOrder {
	
	private final String task =  
			"identifier\n" + 
			"  name\n" + 
			"  version\n" + 
			"muName\n" + 
			"sessionName\n" + 
			"sessionVersion\n" + 
			"uprocName\n" + 
			"uprocVersion\n" + 
			"template\n" + 
			"functionalVersion\n" + 
			"label\n" + 
			"active\n" + 
			"simulated\n" + 
			"type\n" + 
			"code\n" + 
			"index\n" + 
			"specificData\n" + 
			"implicitData\n" + 
			"functionalVersion\n" + 
			"label\n" + 
			"periodType\n" + 
			"periodNumber\n" + 
			"weekAuthorization\n" + 
			"workedDays\n" + 
			"closedDays\n" + 
			"blankDays\n" + 
			"monthAuthorization\n" + 
			"authorizations\n" + 
			"direction\n" + 
			"yearAuthorization\n" + 
			"authorizations\n" + 
			"pattern\n" + 
			"positionInPeriod\n" + 
			"positionsInPeriod\n" + 
			"forward\n" + 
			"positionsPattern\n" +  
			"dayAuthorizations\n" + 
			"exitPeriodAllowed\n" + 
			"referenceDate\n" + 
			"firstScheduledDate\n" + 
			"endDate\n" + 
			"authorized\n" + 
			"internal\n" + 
			"optional\n" + 
			"launchHourPatterns\n" + 
			"startTime\n" + 
			"frequency\n" + 
			"durationHour\n" + 
			"durationMinute\n" + 
			"durationSecond\n" + 
			"generateEvent\n" + 
			"validFrom\n" + 
			"autoRestart\n" + 
			"central\n" + 
			"endExecutionForced\n" + 
			"parallelLaunch\n" + 
			"userName\n" + 
			"printer\n" + 
			"queue\n" + 
			"priority\n" + 
			"dayOffset\n" + 
			"typeDayOffset\n" + 
			"unitOffset\n" + 
			"functionalPeriod\n" + 
/*			"  unit\n" + 
			"  count\n" + 
			"  index\n" + */ 
			"isDeployFlag\n" + 
			"isUpdateFlag\n" + 
			"isInteractiveFlag\n" + 
			"deployDate\n" + 
			"history\n" + 
			"isUprocHeader\n" + 
			"originNode\n" + 
			"flagAdvance\n" + 
			"advanceDays\n" + 
			"advanceHours\n" + 
			"advanceMinutes";
	
	private final String uproc = 
			"identifier\n" + 
			"  name\n" + 
			"  version\n" + 
			"  current\n" + 
			"functionalVersion\n" + 
			"currentVersionField\n" + 
			"label\n" + 
			"type\n" + 
			"referenceType\n" + 
			"  external\n" + 
			"  internal\n" + 
			"  internalScript\n" + 
//			"    uproc\n" + 
			"    lines\n" + 
			"    isBinary\n" + 
			"application\n" + 
			"domain\n" + 
			"interactive\n" + 
			"variables\n" + 
			"defaultInformation\n" + 
			"defaultSeverity\n" + 
			"enhancedStatus\n" + 
			"specificData\n" + 
			"icon\n" + 
			"files\n" + 
			"addList\n" + 
			"removeList\n" + 
			"originalList\n" + 
			"autopatch\n" + 
			"dependencies\n" + 
/*			"  num\n" +   //2 more indent spaces . does not work because it is repeated
			"  uproc\n" + 
			"  status\n" + 
			"  userControl\n" + 
			"  expected\n" + 
			"  fatal\n" + 
			"  sessionControl\n" + 
//			"    type\n" + 
			"  muControl\n" + 
//			"    type\n" + 
			"  oneEnough\n" + 
			"  functionalPeriod\n" + 
			"  pdateCtrl\n" + 
			"  unitDay\n" +
*/			
/*			"fp\n" + 
			"operator\n" + 
			"value\n" + 
			"outer-class\n" +  
			"  unitMonth\n" + */
/*			"fp\n" + 
			"unit\n" + 
			"count\n" + 
			"index\n" + 
			"operator\n" + 
			"value\n" + 
			"outer-class\n" +  
			"  unitYear\n" + */
/*			"fp\n" + 
			"unit\n" + 
			"count\n" + 
			"index\n" + 
			"operator\n" + 
			"value\n" + 
			"outer-class\n" +  
			"  nonSimultaneities\n" + 
			"  resources\n" + */
			"formula\n" + 
			"formulaText\n" + 
			"functionalPeriod\n" + 
/*			"unit\n" + 
			"count\n" + 
			"index\n" + */ 
			"memorization\n" + 
			"  type\n" + 
			"  number\n" + 
			"successors\n" + 
			"uprocClass\n" + 
			"incompatibilities\n" + 
			"completionInstructions";
	
	
	public final String session = "";
	
	public final String resource = "";
	
	private HashMap<String,String> taskFields = new HashMap<String,String>();
	private HashMap<String,String> uprocFields = new HashMap<String,String>();
	private HashMap<String,String> sessionFields = new HashMap<String,String>();
	private HashMap<String,String> resourceFields = new HashMap<String,String>();
	
	public FieldOrder() {
		String[] tokens = task.split("\\\n");
		int idx=1;
		for(String t : tokens) {
			taskFields.put(t, String.format("%02d", idx));
			idx++;
		}
		
		tokens = uproc.split("\\\n");
		idx=1;
		for(String t : tokens) {
			uprocFields.put(t, String.format("%02d", idx));
			idx++;
		}
		
		tokens = session.split("\\\n");
		idx=1;
		for(String t : tokens) {
			sessionFields.put(t, String.format("%02d", idx));
			idx++;
		}
		
		tokens = resource.split("\\\n");
		idx=1;
		for(String t : tokens) {
			resourceFields.put(t, String.format("%02d", idx));
			idx++;
		}
	}

	private enum Type {TASK,UPROC,SESSION,RESOURCE,NONE};
	
	public String reduce(String s) {
		/* DEBUG
		System.out.println("OBJECT ---------------------------------------\n" 
					+ s
					+ "\n---------------------------------------\n" );
		*/
		String[] tokens = s.split("\\\n");
		String result="";
		
		boolean skipCriterion=false;
		boolean skipRule=false;
		boolean skipXML=false;
		
		for (int i = 0; i < tokens.length; i++) {
			String t = tokens[i];
			boolean skip=false;			
			if (t.trim().endsWith(":"))
				if (i < tokens.length-1) { 
					if (!t.contains("dependencies")){
						String nextT = tokens[i+1];
						if (sameIndent(t,nextT))
							skip=true;
					}
				}
				else
					skip=true;
			
			//check this first to avoid side effect of the bool var assignment
			if (skipCriterion)
				if (!t.startsWith("      ")) 
					skipCriterion=false;
			if (skipRule)
				if (!t.startsWith("          ")) 
					skipRule=false;			
			if (skipXML)
				if (!t.startsWith("      ")) 
					skipXML=false;
			
			if (t.startsWith("    criterion:")) {
				skip=true;
				skipCriterion=true;
			}
			else
			if (t.startsWith("      implicitData:")) {
				skip=true;
				skipRule=true;
			}
			else
			if (t.startsWith("    data:") && i<tokens.length-2 && tokens[i+2].startsWith("        <?xml version")) { 
				skip=true;
				skipXML=true;
			}
		
			if (!skip && !skipCriterion &&!skipXML && !(skipRule && !(t.trim().startsWith("name") || t.trim().startsWith("identifier"))))
				result +=t+"\n";
			/* DEBUG
			else			
				System.out.println("skipped " + t);
			 */
		}
		
		result = result.replaceAll("\\n +true"," true");
		result = result.replaceAll("\\n +false"," false");
		
		return result;
	}
	
	private boolean sameIndent(String t, String nextT) {
		int indent1=0;
		int indent2=0;
		for (int i = 0; i < t.length(); i++) {			
			if (t.charAt(i)!=' ') {
				indent1=i;
				break;
			}
		}
		for (int i = 0; i < nextT.length(); i++) {			
			if (nextT.charAt(i)!=' ') {
				indent2=i;
				break;
			}
		}
		return indent1==indent2;
	}

	public String sort(String s) {
		Type type = Type.NONE;
		String[] tokens = s.split("\\\n");
		
		TreeMap<String,String> orderedLines = new TreeMap<String,String>();
		
		String result="";
		String lastFieldIdx = "00";		
				
		for (String t : tokens) {
			String firstToken = t.split(":")[0].replaceFirst("    ", "");
			String fieldIdx = null;
			switch (type) {
				case NONE:		if (firstToken.equals("Task")) 
									type = Type.TASK;
								else
								if (firstToken.equals("Uproc")) 
									type = Type.UPROC;
								else
								if (firstToken.equals("Session")) 
									type = Type.SESSION;
								else
								if (firstToken.equals("Resource")) 
									type = Type.RESOURCE;
				
								break;
								
				case UPROC:		fieldIdx = uprocFields.get(firstToken);
								break;								
				case TASK:		fieldIdx = taskFields.get(firstToken);
								break;				
				case SESSION:   fieldIdx = sessionFields.get(firstToken);
								break;
				case RESOURCE:	fieldIdx = resourceFields.get(firstToken);
								break;
			}
			
			if (fieldIdx==null)
				fieldIdx=lastFieldIdx;
			else
				lastFieldIdx=fieldIdx;
			if (orderedLines.containsKey(fieldIdx)) {
				String str = orderedLines.get(fieldIdx);
				orderedLines.put(fieldIdx, str+"\n"+t);
			}
			else
				orderedLines.put(fieldIdx, t);
		}
		
		for (String str: orderedLines.values())
			result+=str+"\n";
		
		if (result.length()>0)
			result = result.substring(0,result.length()-1);
		
		return result;
	}
}